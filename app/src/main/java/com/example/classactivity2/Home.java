package com.example.classactivity2;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
public class Home extends AppCompatActivity {
    private Button btn;
    private View.OnClickListener btnOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            awesomeButtonClicked();
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home2);
        btn = (Button) findViewById(R.id.button);
        btn.setOnClickListener(btnOnClickListener);
    }
    private void awesomeButtonClicked() {
        Intent intent = new Intent(Home.this, GoodBye.class);
        startActivity(intent);
    }


}